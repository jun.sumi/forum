package br.com.jjss.forum.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.jjss.forum.controller.dto.DetalhesTopicoDto;
import br.com.jjss.forum.controller.dto.TopicoDto;
import br.com.jjss.forum.controller.form.AtualizacaoTopicoForm;
import br.com.jjss.forum.controller.form.TopicoForm;
import br.com.jjss.forum.model.Topico;
import br.com.jjss.forum.repository.CursoRepository;
import br.com.jjss.forum.repository.TopicoRepository;

@RestController
@RequestMapping("/topicos")
public class TopicosController {

	@Autowired
	private TopicoRepository topicoRepository;
	
	@Autowired
	private CursoRepository cursoRepository;
	
	@GetMapping
	@Cacheable(value = "listaDeTopicos")
	public Page<TopicoDto> lista(@RequestParam(required = false) String nomeCurso, @PageableDefault(sort="id", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) {
		if (nomeCurso == null || "".equals(nomeCurso.trim())) {
			return TopicoDto.converter(topicoRepository.findAll(paginacao));
		}
		return TopicoDto.converter(topicoRepository.findByCursoNome(nomeCurso, paginacao));
	}
	
	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<TopicoDto> cadastrar(@RequestBody @Valid TopicoForm form, UriComponentsBuilder uriBuilder) {
		Topico topico = form.converter(cursoRepository);
		topico = topicoRepository.save(topico);
		
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
		return ResponseEntity.created(uri).body(new TopicoDto(topico));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesTopicoDto> detalhar(@PathVariable("id") Long id) {
		Optional<Topico> topico = this.topicoRepository.findById(id);
		if (!topico.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(new DetalhesTopicoDto(topico.get()));
	}
	
	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<TopicoDto> atualizar(@PathVariable("id") Long id, @RequestBody @Valid AtualizacaoTopicoForm form, UriComponentsBuilder uriBuilder) {
		Optional<Topico> optTopico = this.topicoRepository.findById(id);
		if (!optTopico.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Topico topico = optTopico.get();
		topico.setTitulo(form.getTitulo());
		topico.setMensagem(form.getMensagem());
		
		return ResponseEntity.ok(new TopicoDto(topico));
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		Optional<Topico> optTopico = this.topicoRepository.findById(id);
		if (!optTopico.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		topicoRepository.delete(optTopico.get());
		return ResponseEntity.ok().build();
	}
}
