package br.com.jjss.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import br.com.jjss.forum.model.StatusTopico;
import br.com.jjss.forum.model.Topico;
import lombok.Data;

@Data
public class DetalhesTopicoDto {

	private Long id;
	private String titulo;
	private String mensagem;
	private LocalDateTime dataCriacao;
	private String nomeAutor;
	private StatusTopico status;
	private List<RespostaDto> respostas;

	public DetalhesTopicoDto(Topico topico) {
		super();
		this.id = topico.getId();
		this.titulo = topico.getTitulo();
		this.mensagem = topico.getMensagem();
		this.dataCriacao = topico.getDataCriacao();
		this.nomeAutor = topico.getAutor() != null?  topico.getAutor().getNome() : null;
		this.status = topico.getStatus();
		if (topico.getRespostas() != null) {
			this.respostas = topico.getRespostas().stream().map(RespostaDto::new).collect(Collectors.toList());
		}
	}


}
