package br.com.jjss.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jjss.forum.model.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {

	Curso findByNome(String nome);

}
