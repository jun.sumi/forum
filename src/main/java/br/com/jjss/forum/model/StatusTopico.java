package br.com.jjss.forum.model;

public enum StatusTopico {
	
	NAO_RESPONDIDO,
	NAO_SOLUCIONADO,
	SOLUCIONADO,
	FECHADO;

}
