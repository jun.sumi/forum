package br.com.jjss.forum.config.validacao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.jjss.forum.config.exception.CustomBadCredentionException;

@RestControllerAdvice
public class BadCredentialsHandler {

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(CustomBadCredentionException.class)
	public ErrorMessage handle(CustomBadCredentionException exception) {	
		return new ErrorMessage(exception.getMessage());
	}
}
