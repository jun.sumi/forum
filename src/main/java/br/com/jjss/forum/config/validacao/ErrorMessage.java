package br.com.jjss.forum.config.validacao;

public class ErrorMessage {

	public String erro;

	public ErrorMessage(String erro) {
		super();
		this.erro = erro;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
	
}
