package br.com.jjss.forum.config.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class CustomBadCredentionException extends BadCredentialsException {

	private static final long serialVersionUID = -7103045446593914529L;

	public CustomBadCredentionException(String msg, Throwable t) {
		super(msg, t);
	}

	public CustomBadCredentionException(String msg) {
		super(msg);
	}

}
