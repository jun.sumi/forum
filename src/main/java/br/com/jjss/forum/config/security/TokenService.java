package br.com.jjss.forum.config.security;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.jjss.forum.config.exception.CustomBadCredentionException;
import br.com.jjss.forum.controller.form.LoginForm;
import br.com.jjss.forum.model.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	@Value("${forum.jwt.expiration}")
	private String expiration;
	
	@Value("${forum.jwt.secret}")
	private String secret;
	
	@Autowired
	private PasswordEncoder encoder;
	
	private void validarSenha(LoginForm loginForm, Usuario logado) {
		if (!encoder.matches(loginForm.getSenha(), logado.getPassword())) {
			throw new CustomBadCredentionException("Usuário/senha inválido!");
		}
	}
	
	public String gerarToken(LoginForm loginForm, Authentication authentication) {
		Usuario logado = (Usuario) authentication.getPrincipal();
		validarSenha(loginForm, logado);;
		Calendar dataCriacao = Calendar.getInstance();
		Calendar dataExpiracao = Calendar.getInstance();
		dataExpiracao.add(Calendar.MILLISECOND, Integer.parseInt(expiration));
		
		return Jwts.builder()
				.setIssuer("Forum API")
				.setSubject(String.valueOf(logado.getId()))
				.setIssuedAt(dataCriacao.getTime())
				.setExpiration(dataExpiracao.getTime())
				.signWith(SignatureAlgorithm.HS256, secret)//criptografia do token
				.compact()
				;
	}

	public boolean isTokenValido(String token) {
		try {
			Jwts.parser()
				.setSigningKey(this.secret)
				.parseClaimsJws(token);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	public Long getIdUsuario(String token) {
		Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		return Long.parseLong(body.getSubject());
	}

	
}
